/*
 * Copyright (c) 2018 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.companion.idea.linemarker;

import com.intellij.codeInsight.daemon.GutterIconNavigationHandler;
import com.intellij.codeInsight.daemon.GutterMark;
import com.intellij.codeInsight.daemon.LineMarkerInfo;
import com.intellij.codeInsight.navigation.NavigationGutterIconRenderer;
import com.intellij.psi.presentation.java.SymbolPresentationUtil;
import com.intellij.testFramework.UsefulTestCase;
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase;
import com.intellij.util.containers.ContainerUtil;
import eu.andret.ats.companion.idea.utilities.IconProvider;

public class TypeFallbackLineMarkerProviderTest extends LightJavaCodeInsightFixtureTestCase {
	@Override
	protected String getTestDataPath() {
		return "src/test/testData/linemarker";
	}

	public void testArgumentFallbackLineMarkerProvider() {
		// given
		myFixture.configureByFile("type-fallback-line-marker.java");
		myFixture.addClass("package org.bukkit.player; public class Player {}");
		myFixture.addClass("package org.bukkit.command; public class CommandSender {}");
		myFixture.addClass("package org.bukkit.plugin.java; public class JavaPlugin {}");

		// when
		final GutterMark gutterMark = myFixture.findGutter("type-fallback-line-marker.java");

		// then
		assertNotNull(gutterMark);
		assertEquals("Find @TypeFallback method", gutterMark.getTooltipText());
		assertEquals(IconProvider.FALLBACK, gutterMark.getIcon());

		if (!(gutterMark instanceof LineMarkerInfo.LineMarkerGutterIconRenderer)) {
			throw new IllegalArgumentException(gutterMark.getClass() + ": gutter not supported");
		}
		final LineMarkerInfo.LineMarkerGutterIconRenderer<?> renderer =
				UsefulTestCase.assertInstanceOf(gutterMark, LineMarkerInfo.LineMarkerGutterIconRenderer.class);
		final LineMarkerInfo<?> lineMarkerInfo = renderer.getLineMarkerInfo();
		final GutterIconNavigationHandler<?> handler = lineMarkerInfo.getNavigationHandler();

		if (!(handler instanceof final NavigationGutterIconRenderer iconRenderer)) {
			throw new IllegalArgumentException(handler + ": handler not supported");
		}
		UsefulTestCase.assertSameElements(
				ContainerUtil.map(iconRenderer.getTargetElements(), SymbolPresentationUtil::getSymbolPresentableText),
				"fallbackTest(String)");
	}

	public void testArgumentFallbackLineMarkerProviderInLambda() {
		// given
		myFixture.configureByFile("type-fallback-line-marker-in-lambda.java");
		myFixture.addClass("package org.bukkit.player; public class Player {}");
		myFixture.addClass("package org.bukkit.command; public class CommandSender {}");
		myFixture.addClass("package org.bukkit.plugin.java; public class JavaPlugin {}");

		// when
		final GutterMark gutterMark = myFixture.findGutter("type-fallback-line-marker-in-lambda.java");

		// then
		assertNull(gutterMark);
	}
}
